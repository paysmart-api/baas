---
title: Códigos de Erro da API
tags: 
 - codigos de erro
description: Códigos de erros com status code 409 retornados pela API. 
---

# Códigos de erro com status code 409
Tabela com os possíveis erros retornados pela API.

### `POST` /account/{accountId}/transfer

| Código | Descrição |
| ------ | ------ |
| insufficient.funds | Saldo insuficiente para realizar a transferência |
| account.not.found | Conta não encontrada |
| account.recipient.not.found | Conta de destino não encontrada |

### `POST` /account/{accountId}/internal-transfer

| Código | Descrição |
| ------ | ------ |
| insufficient.funds | Saldo insuficiente para realizar a transferência |
| account.not.found | Conta não encontrada |
| account.recipient.not.found | Conta de destino não encontrada |


### `POST` /account/{accountId}/transfer/batch

| Código | Descrição |
| ------ | ------ |
| account.not.found | Conta não encontrada |
| account.not.participate.arrangement | Conta não esta no arranjo |
| invalid.expire.date | Data de expiração inválida |
| account.recipient.not.found | Conta de destino não encontrada |
| account.does.not.have.enough.balance | Saldo insuficiente para realizar a transferência |

### `POST` /account/{accountId}/link

| Código | Descrição |
| ------ | ------ |
| account.not.found | Conta não encontrada |
| invalid.main.account | Conta principal de arranjo inválida |
| account.already.linked | Conta já adicionada no arranjo |
| account.client.is.main | Conta cliente é principal de uma conta |
| invalid.client.account | Conta cliente não elegivel para arranjo |
| account.with.balance | Conta com saldo pendente não pode ser adicionada no arranjo |

### `POST` /account/unlink

| Código | Descrição |
| ------ | ------ |
| account.not.found | Conta não encontrada |
| account.already.unliked | Conta cliente ja foi removida do arranjo |

### `POST` /account//{accountId}/cash-in

| Código | Descrição |
| ------ | ------ |
| account.not.found | Conta não encontrada |

### `POST` /account//{accountId}/cash-out

| Código | Descrição |
| ------ | ------ |
| account.not.found | Conta não encontrada |
| insufficient.funds | Saldo insuficiente para realizar a operação |

### `POST` /account

| Código | Descrição |
| ------ | ------ |
| account.already.exists | Conta já cadastrada |
| invalid.main.account | Conta principal de arranjo inválida |

### `GET` /account/{accountId}/transfer/batch/{processingCode}

| Código | Descrição |
| ------ | ------ |
| transfer.not.found | Transfência não encontrada |

### `GET` /accounts

| Código | Descrição |
| ------ | ------ |
| list.needs.parameter | Pelo menos um parâmetro é necessário para efetuar a busca |

### `POST` /account/company

| Código | Descrição |
| ------ | ------ |
| account.already.exists | Conta já cadastrada |
| invalid.main.account | Conta principal de arranjo inválida |

### `PUT` /account/{accountId}/credits/expiration

| Código | Descrição |
| ------ | ------ |
| id.xor.processing.code.is.required | Id ou código de processamento devem ser enviados (não os 2 juntos) |
| transaction.expiration.not.found | Transação não encontrada |

### `GET` /pix/key/{document}/{key}

| Código | Descrição |
| ------ | ------ |
| key.not.found | Chave pix não encontrada |
| account.temporarily.blocked.this.operation | Conta bloqueada temporariamente por excesso de leitura |

### `POST` /pix/payment

| Código | Descrição |
| ------ | ------ |
| e2e.addressing.key.required | Com e2e o campo 'recipientPixKey' é obrigatório |
| e2e.institution.code.required | Com e2e o campo 'recipientInstitutionCode' é obrigatório |
| e2e.account.number.required | Com e2e o campo 'recipientAccountNumber' é obrigatório |
| e2e.account.type.required | Com e2e o campo 'recipientAccountType' é obrigatório |
| e2e.cpf.cnpj.required | Com e2e o campo 'recipientCpfCnpj' é obrigatório |

### `POST` /pix/payment/cancel/schedule

| Código | Descrição |
| ------ | ------ |
| scheduling.not.in.valid.date | O agendamento esta em uma data inválida  para efetuar o cancelamento |
| scheduling.not.found | Agendamento não encontrado |

### `POST` /pix/payment/chargeback

| Código | Descrição |
| ------ | ------ |
| transaction.not.found | Transação não encontrada |
| transaction.not.belong.account | A transação não pertence a conta |
| transaction.not.receipt | A transação não é do tipo "recebimento" |
| payee.account.not.found | Conta do beneficiário não encontrada |

### `POST` /pix/qrcode/generate/static

| Código | Descrição |
| ------ | ------ |
| description.identifier.length | Tamanho do campo descrição + identificador inválido |

### `POST` /pix/qrcode/read

| Código | Descrição |
| ------ | ------ |
| qr.code.expired | QR code expirado |
| invalid.qr.code | Valor do QR code inválido |


