---
title: Erros gerais a serem tratados
tags: 
 - codigos de erro
description: Erros gerais a serem tratados
---

# Erros gerais a serem tratados

### Status code 400 - Campo inválidos na requisição
Estrutura de exemplo:
```
[
  {
    "code": "invalid.field",
    "field": "recipientInstitutionCode",
    "message": "must not be blank"
  }
]
```
O campo `code` vem com o valor `invalid.field`, indicando campos inválidos.

O campo `field` indica qual o campo esta com erro.

O campo `message` mostra uma descrição do erro.

### Status code 401 - Erro com Api key
Estrutura de exemplo:
```
{
  "code": "api.key.invalid"
}
```
O campo `code` indica o código do erro. Podem vir os valores:

| Código | Descrição |
| ------ | ------ |
| method.not.available | Método solicitado não esta disponível |
| issuer.not.found | Emissor não encontrado |
| api.key.invalid | Api Key não enviada ou inválida |

### Status code 403 - Feature não habilitada.
Estrutura de exemplo:
```
{
  "code": "api.key.invalid"
}
```
O campo `code` indica o código do erro. Podem vir os valores:

| Código | Descrição |
| ------ | ------ |
| issuer.not.enabled | Emissor não habilitado |
| feature.not.enable | Recurso não habilitado para o emissor |

### Status code 409 - Erro de regra de negócio. Verificar o campo code.
Estrutura de exemplo:
```
{
  "code": "scheduling.not.in.valid.date",
  "message": "A data do agendamento deve ser posterior a data atual"
}
```
O campo `code` indica o código do erro

O campo `message` indica uma descrição do erro.

Verificar tabela com os todos os erros 409.

### Status code 500 - Erro não tratado.
Estrutura de exemplo:
```
{
  "code": "unknow.error",
  "message": "Erro desconhecido a efetuar a transação"
}
```
O campo `code` vem com o código `unknow.error`. 

O campo `message` contém uma descrição adicional.

### Status code 503 - Erro não tratado de integração.
Estrutura de exemplo:
```
{
  "code": "integration.error",
  "message": "Não foi possível efetuar a devolução"
}
```
O campo `code` vem com o código `integration.error`. 

O campo `message` contém uma descrição adicional.