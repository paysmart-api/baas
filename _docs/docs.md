---
title: Visão Geral
# tags: 
#  - fluxo geral
#  - figuras
description: Visão geral da documentação
permalink: /
---

# Visão Geral

Nesta seção, é apresentada uma visão geral da documentação, detalhes sobre a integração e formato das requisições

## 1 Integração

A integração com o serviço é realizada através do consumo de *API's* REST seguindo o protocolo HTTPS. Para isso, se faz necessário a utilização do método de autenticação mTLS (TLS mútuo) através de certificados e chaves disponibilizados pela **Paysmart**.

## 2 Autenticação

Todas as requisições devem ter a chave de api configurada. 

Para isso deve-se usar o header `Api-Key` com a chave fornecida. Por exemplo `"Api-Key"="33134c18-3dc9-4c9d-acba-9d6846553b12"`.

