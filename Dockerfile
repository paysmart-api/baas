FROM ruby:2.7.4

RUN mkdir /opt/baas
WORKDIR /opt/baas

RUN apt-get update
RUN apt-get install build-essential

COPY Gemfile Gemfile.lock  ./
RUN gem update --system
RUN gem install bundler:2.3.15
RUN bundle _2.3.15_ install

COPY . .

EXPOSE 4000

CMD ["bundle","exec","jekyll","serve"]
