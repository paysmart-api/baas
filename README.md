# Documentação do Projeto Conta de Pagamentos

## 1 Estrutura

O arquivo que identifica a estrutra hierárquica de títulos e subtítulos é o `_data/toc.yml`.

A estrutura é basicamennte formada por `title \ url`, logo, todo atributo `title` possui um link que é referenciado no atributo `url`. Os subtítulos são referenciados em `links`, seguidos por uma lista composta pela mesma estrutura de `title \ url`.

## 1 Home

A home está contida no arquivo `pages/index.md`.

## 2 Pages

O diretório `pages` é destinado para todas as páginas que estão relacionadas às ***URL's*** apresentadas no arquivo `_data/toc.yml`.

Nas páginas do projeto, utiliza-se o header abaixo:

``` json
    ---
    layout: page
    title: Docsy Jekyll Theme
    permalink: /
    ---
```

o link que será vinculado à ***URL*** definida no `_data/toc.yml` é definido no atributo `permalink` do header.

## API

A api está especificada no arquivo `_api/openapi-tsp.json`.


## Run

Para rodar localmente

```
bundle exec jekyll serve --livereload
```
